# design patterns

slide gs: https://docs.google.com/presentation/d/11NxhzaDkYWadjhdaWlT67gY2pZ0uPy8b/edit#slide=id.p1

# tổng quan

có 23 mẫu design pattern đã đc công nhận
chia thành 3 nhóm
- Creational Pattern( nhóm khởi tạo):
    +Factory Method
    +Abstract Factory
    +Builder
    +Prototype
    +Singleton

- Structural Pattern (nhóm cấu trúc):
    +Adapter
    +Bridge
    +Composite
    +Decorator
    +Facade
    +Flyweight
    +Proxy

- Behavioral Pattern (nhóm tương tác/hành vi)
    +Interpreter
    +Template Method
    +Chain of Responsibility
    +Command
    +Iterator
    +Mediator
    +Memento
    +Observer
    +State
    +Strategy || ok
    +Visitor


# chi tiết
################################
- Creational Pattern: là một nhóm các design pattern trong kỹ thuật phần mềm. Chúng tập trung vào cách tạo đối tượng một cách linh hoạt, hiệu quả và tránh được sự phụ thuộc giữa các class trong hệ thống.
//Factory Method :Pattern này tạo ra đối tượng một cách gián tiếp bằng cách sử dụng một phương thức tạo đối tượng trong một interface hoặc abstract class. Điều này cho phép việc tạo đối tượng được tách biệt khỏi việc sử dụng đối tượng.
//Abstract Factory Pattern: Pattern này cung cấp một cách để tạo ra một nhóm các đối tượng liên quan hoặc có liên quan nhau một cách đồng bộ. Nó sử dụng một interface hoặc abstract class để tạo các đối tượng, vì vậy các class cụ thể của đối tượng có thể được tạo một cách độc lập.
//Singleton Pattern: Pattern này đảm bảo rằng chỉ có một phiên bản của một lớp được tạo và cho phép truy cập đến nó từ bất kỳ đâu trong hệ thống. Nó đảm bảo rằng lớp đó không thể được khởi tạo nhiều lần và chỉ trả về một đối tượng duy nhất.
//Builder Pattern: Pattern này cho phép bạn tạo ra các đối tượng phức tạp bằng cách sử dụng một số bước xây dựng đối tượng khác nhau. Một builder được sử dụng để xây dựng các thành phần của đối tượng, và sau đó sử dụng chúng để tạo ra đối tượng cuối cùng.
//Prototype Pattern: Pattern này cho phép tạo ra các đối tượng mới bằng cách sao chép các đối tượng đã có. Điều này cho phép bạn tạo các đối tượng mới mà không cần phải gán lại tất cả các giá trị của chúng, giảm thiểu số lượng mã lặp lại và tối ưu hóa việc tạo đối tượng.

#################################
-Structural Pattern :  là một nhóm các design pattern trong kỹ thuật phần mềm. Nhóm này tập trung vào việc cấu trúc lại các class và đối tượng trong hệ thống để giải quyết các vấn đề liên quan đến sự liên kết và phụ thuộc giữa các class.
//Adapter Pattern: Pattern này giúp chuyển đổi giao diện của một class thành một giao diện khác mà client mong muốn sử dụng. Điều này giúp các class hoạt động với nhau một cách linh hoạt hơn.
//Bridge Pattern: Pattern này tách riêng phần trừu tượng của một class và phần cài đặt của nó để có thể thay đổi một cách độc lập. Điều này cho phép các class hoạt động với nhau một cách linh hoạt hơn và giảm thiểu sự phụ thuộc.
//Composite Pattern: Pattern này cho phép xây dựng các cấu trúc cây đối tượng phức tạp bằng cách sử dụng các đối tượng con và đối tượng cha. Điều này giúp tạo ra các cây đối tượng phức tạp và giảm thiểu sự phụ thuộc giữa các đối tượng.
//Decorator Pattern: Pattern này cho phép thêm các tính năng mới cho một class bằng cách sử dụng một class decorator. Điều này cho phép thêm tính năng mà không làm thay đổi cấu trúc của class gốc.
// Facade Pattern: Pattern này cung cấp một interface đơn giản để tương tác với một hệ thống phức tạp. Nó che giấu sự phức tạp của hệ thống đằng sau một interface đơn giản và giúp giảm thiểu sự phụ thuộc giữa client và hệ thống.
//Flyweight Pattern: Pattern này giúp tối ưu hóa việc sử dụng bộ nhớ bằng cách chia sẻ các đối tượng giống nhau giữa các class khác nhau. Điều này giúp giảm thiểu sự phụ thuộc và tối ưu hóa bộ nhớ của hệ thống.


#################################
Behavioral Pattern :là một nhóm các design pattern trong kỹ thuật phần mềm. Nhóm này tập trung vào việc quản lý hành vi của các đối tượng và cách chúng tương tác với nhau.
//Chain of Responsibility Pattern: Pattern này cho phép xử lý một yêu cầu theo một chuỗi các đối tượng xử lý. Mỗi đối tượng có thể xử lý yêu cầu hoặc chuyển nó cho đối tượng tiếp theo trong chuỗi.
//Command Pattern: Pattern này cho phép đóng gói một yêu cầu thành một đối tượng và truyền nó cho một đối tượng khác để thực hiện. Điều này cho phép giảm thiểu sự phụ thuộc giữa các đối tượng và cung cấp khả năng hoàn tác.
//Interpreter Pattern: Pattern này giúp giải quyết các vấn đề liên quan đến việc hiểu và thực hiện các biểu thức ngôn ngữ. Nó định nghĩa một ngôn ngữ và cung cấp một cách để đánh giá các biểu thức của nó.
//Iterator Pattern: Pattern này cho phép truy cập các phần tử của một tập hợp một cách tuần tự mà không cần tiết lộ chi tiết cách tập hợp được triển khai.
//Mediator Pattern: Pattern này cung cấp một cách để các đối tượng tương tác với nhau mà không cần trực tiếp liên lạc với nhau. Thay vào đó, chúng sử dụng một đối tượng trung gian để xử lý các tương tác giữa chúng.
//Observer Pattern: Pattern này cho phép các đối tượng được thông báo khi trạng thái của một đối tượng khác thay đổi. Nó cho phép các đối tượng tương tác với nhau một cách lỏng lẻo mà không cần sự phụ thuộc chặt chẽ giữa chúng.
//State Pattern: Pattern này cho phép một đối tượng thay đổi hành vi của nó khi trạng thái của nó thay đổi. Nó cung cấp một cách để quản lý các trạng thái khác nhau của một đối tượng và hành vi của nó tương ứng với mỗi trạng thái đó.
//Strategy Pattern: Pattern này cho phép lựa chọn giữa các thuật toán khác nhau để giải quyết một vấn đề cụ thể. Nó cho phép thay đổi thuật toán trong thời gian chạy
//Template Method Pattern: Pattern này cung cấp một khung cho việc triển khai một thuật toán, trong đó các bước cơ bản được định nghĩa trong một phương thức, và các bước cụ thể được triển khai trong các lớp con.
//Visitor Pattern: Pattern này cho phép tách rời phần xử lý của đối tượng từ cấu trúc của nó. Nó cho phép các đối tượng được truy cập một cách độc lập, mà không cần biết cấu trúc của chúng, bằng cách cung cấp một đối tượng truy cập để thực hiện các hoạt động trên chúng.
//Memento Pattern: Pattern này cho phép lưu trữ và khôi phục trạng thái của một đối tượng mà không làm lộ chi tiết cấu trúc của nó. Nó cho phép một đối tượng quản lý lịch sử của trạng thái của nó, và khôi phục lại trạng thái trước đó nếu cần thiết.
//Null Object Pattern: Pattern này cung cấp một đối tượng giả để đại diện cho một đối tượng không tồn tại hoặc không được khởi tạo. Điều này giúp giảm thiểu các kiểm tra null và đảm bảo rằng phần mềm của bạn sẽ không gặp lỗi khi thao tác với đối tượng không tồn tại.
//Command Query Separation Pattern: Pattern này đề xuất tách riêng việc thực hiện các thao tác (command) và truy vấn (query) trên một đối tượng. Điều này giúp đảm bảo tính toàn vẹn dữ liệu và tránh các xung đột đồng thời khi nhiều đối tượng cùng thao tác với cùng một đối tượng.
//Dependency Injection Pattern: Pattern này giúp giảm sự phụ thuộc giữa các đối tượng bằng cách cung cấp các phụ thuộc của chúng thông qua constructor hoặc các phương thức khác. Điều này giúp tăng tính linh hoạt của phần mềm và giảm thiểu sự phụ thuộc giữa các đối tượng.

