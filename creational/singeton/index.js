const Singleton = {
    instance: null,
    getInstance() {
        if (!this.instance) {
            this.instance = { new: "instance" }
        }
        return this.instance
    }
}
